from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.views import APIView
from rest_framework import generics

from rest_framework import authentication, permissions
from django.contrib.auth.models import User

from .serializers import ProductSerializer, ProductWallSerializer
from blog.models import Product, ProductWall
from django.http import Http404
from rest_framework.response import Response
from rest_framework import status

class ProductViewSet(viewsets.ModelViewSet):
    authentication_classes = [SessionAuthentication, BasicAuthentication, TokenAuthentication]
    permission_classes = [IsAuthenticated, IsAdminUser]

    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    lookup_field = 'slug'

    def create(self, request, *args, **kwargs):
        request.data['author'] = request.user.id
        return super().create(request, *args, **kwargs)

class ProductWallViewSet(viewsets.ModelViewSet):
    authentication_classes = [SessionAuthentication, BasicAuthentication, TokenAuthentication]
    permission_classes = [IsAuthenticated, IsAdminUser]

    queryset =  ProductWall
    serializer_class = ProductWallSerializer
    lookup_field = 'slug'

    def retrieve(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset)
        return Response(serializer.data)

    def get_queryset(self):
        slug = self.kwargs['slug']
        if self.action == 'list':
            return Product.objects.get(slug=slug).pwall.all()
        if self.action == 'retrieve':
            return Product.objects.get(slug=slug).pwall.last()
    
    def create(self, request, *args, **kwargs):
        slug = self.kwargs['slug']
        request.data['prod'] = Product.objects.get(slug=slug).id
        return super().create(request, *args, **kwargs)
    
"""
class ProductWallAPI(generics.ListAPIView):
    #authentication_classes = [authentication.TokenAuthentication]
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [permissions.IsAdminUser]

    serializer_class = ProductWallSerializer

    def get_queryset(self):
        slug = self.kwargs['slug']
        return Product.objects.get(slug=slug).pwall.all()


    
    def put(self, request, slug, format=None):
        snippet = self.get_object(slug)
        serializer = ProductWallSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        snippet = self.get_object(slug)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
"""