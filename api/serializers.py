from rest_framework import serializers
from blog.models import Product, ProductWall


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'
        """fields = (
        'title',
        'slug',
        'price',
        'ticker',
        'description_text_only',
        'visible',
        'image',
        'updated_on',
        'created_on',
        )"""

        

class ProductWallSerializer(serializers.ModelSerializer):
    class Meta:
        #prod = ProductSerializer()
        model = ProductWall
        fields = '__all__'
        #depth = 1