from . import views
from django.urls import path, include
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns


#router = routers.DefaultRouter()
#router.register(r'products', views.ProductViewSet)
#router.register(r'products_wall', views.ProductWallViewSet)
#router.register(r'listuser', views.ListUsers, basename='api_listuser')

urlpatterns = format_suffix_patterns ([
 #   path('', include(router.urls)),
    path('v1/products/', views.ProductViewSet.as_view({'get': 'list'})),
    path('v1/product/<slug>/', views.ProductViewSet.as_view({'get': 'retrieve'})),
    path('v1/create/product/', views.ProductViewSet.as_view({'post': 'create'})),
    path('v1/product/<slug>/details/', views.ProductWallViewSet.as_view({'get': 'list'})),
    path('v1/product/<slug>/add/', views.ProductWallViewSet.as_view({'post': 'create'})),
    path('v1/product/<slug>/detail/last/', views.ProductWallViewSet.as_view({'get': 'retrieve'})),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
])