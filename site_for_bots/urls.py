from django.contrib import admin
from django.urls import path, include

#for ckeditor
from django.conf import settings
from django.conf.urls.static import static
#for ckeditor

urlpatterns = [
    path('admin/', admin.site.urls, name = 'admin_page'),
    path('', include('main.urls'), name = 'root'),
    path('accounts/', include('accounts.urls')),
    path('blog/', include('blog.urls')),
    path('forum/', include('forum.urls')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('social/', include('allauth.urls')),
    path('api/', include('api.urls')),
    path('payment/', include('payment.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) #for ckeditor

# для перевода
from django.conf.urls.i18n import i18n_patterns
urlpatterns += i18n_patterns(
    path('', include('main.urls')),
    path('accounts/', include('accounts.urls')),
    path('blog/', include('blog.urls')),
    path('forum/', include('forum.urls')),
    path('social/', include('allauth.urls')),
    path('api/', include('api.urls')),
    path('payment/', include('payment.urls')),
)