from django import template
from ..models import Profile
from blog.models import Comments
from main.models import Message
from django.utils.html import mark_safe #gg
import json
from django.contrib.auth.models import User
register = template.Library()
    
@register.filter()
def get_avatar(id):
    return Profile.objects.get(id=id).avatar

@register.filter()
def get_chat_messages(id, to_user):
    id = int(id)
    to_user = int(to_user)
    user0 = Profile.objects.get(id=to_user)
    user1 = Profile.objects.get(id=id)
    if user1.friends.filter(id=user0.id).exists():

        room_id = get_room_id(id, to_user)
        messages = Message.objects.filter(room=room_id)
        msg = {}
        #ii=""
        for counter, i in enumerate(messages):
            mssg = {}
            if str(i).split(',   ')[0] == user1.slug:
                mssg['side'] = True
            mssg['user'] = str(i).split(',   ')[0]
            mssg['msg'] = str(i).replace('\\', '\\\\').split(',   ')[1]
            mssg['date'] = str(i)[:-13].split(',   ')[2]
            msg['msg' + str(counter)] = mssg
        msg['room_id'] = room_id
            #user=str(i).split(',   ')[0]
            #msg=str(i).split(',   ')[1]
            #date=str(i)[:-13].split(',   ')[2]

            #ii += """
            #    <div class="row text-light">
            #        <p align="left" class="text-secondary"> {user} | {date} </p>
            #        <p align="left"> {msg} </p>
            #   </div>
            #""".format(user=user, msg=msg, date=date)
        #print(ii)
        return  json.dumps(msg) #mark_safe(ii)


@register.filter()
def get_uncheck_messages(room_id,username):
    messages = Message.objects.filter(room=room_id,check=False,username=username)
    if len(messages) != 0:
        return mark_safe('<span style="position: absolute; top: 0%; left: 0%;" class="translate-middle badge rounded-pill bg-danger" style="color: cyan;">{}</span>&nbsp;'.format(len(messages)))
    return ''


@register.filter()
def get_room_id(id, to_user):
    room_id0 = id / float(to_user) + float(to_user) / id
    room_id1 = id ** 0.5 + float(to_user) ** 0.5
    room_id = room_id0 + room_id1
    return room_id