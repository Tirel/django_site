# Подключение для рендера
from django.shortcuts import render, redirect

# Подключение новой формы для регистрации
from .forms import RegistrForm
from .models import Profile, Friend_Request, Chat, Files
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
#from django.contrib.auth.decorators import login_required
from .forms import ProfileForm, UserForm, FilesForm
from django.urls import reverse, reverse_lazy

from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from blog.models import Purchases
from django.conf import settings
from main.models import Message

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
#from blog.models import Purchases

from datetime import datetime

def purchases(request):
    queryset_list = Purchases.objects.filter(customer=request.user)
    paginator = Paginator(queryset_list, 10)
    page = request.GET.get('page')
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        queryset = paginator.page(1)
    except EmptyPage:
        queryset = paginator.page(paginator.num_pages)
    date_now = datetime.now()
    context = {
		"object_list": queryset,
        "date_now": date_now
	}
    return render(request, "accounts/purchases.html", context)

def regist(request):
    data = {}
    if request.method == 'POST':
        form = RegistrForm(request.POST)
        if form.is_valid():
            form.save()
            data['form'] = form
            data['res'] = "Всё прошло успешно"
            return HttpResponseRedirect(reverse_lazy("login"))
            #return render(request, 'registration/registr.html', data)
    else:
        form = RegistrForm()
        data['form'] = form
        return render(request, 'registration/registr.html', data)



class FileCreateView(LoginRequiredMixin, generic.CreateView):
    template_name = 'accounts/files.html'
    model = Files
    form_class = FilesForm
    success_url = reverse_lazy('files')

    def get_context_data(self,**kwargs):
        kwargs['files'] = Files.objects.all().filter(owner=self.request.user).order_by('-id')
        return super().get_context_data(**kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        #if not self.request.user.is_superuser and not self.request.user.is_staff:
            #return self.handle_no_permission()
        return kwargs

    def form_valid(self,form):
        self.object = form.save(commit=False)
        self.object.owner = self.request.user
        self.object.save()
        return super().form_valid(form)

class FileDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = Files
    success_url = reverse_lazy('files')

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.request.user != self.object.owner:
            return self.handle_no_permission()
        self.object.delete()
        return redirect('files')

async def profile_edit(request):
    data = {}
    user_p = Profile.objects.get(user=request.user)
    user = User.objects.get(id=request.user.id)
    data['user_p'] = user_p
    data['user'] = user
    if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES, instance=user_p)
        form0 = UserForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            form0.save()
            data['form'] = form
            data['form0'] = form0
            return HttpResponseRedirect(reverse_lazy("profile_edit"))
    else:
        form = ProfileForm(instance=user_p)
        form0 = UserForm(instance=user)
        data['form'] = form
        data['form0'] = form0
        return render(request, 'accounts/edit_profile.html', data)

class profile_view(generic.DetailView):
    #context_object_name = 'profile'
    model = Profile
    template_name = 'accounts/profile.html'


async def account_view(request, slug):
    if request.user.id == None:
        return redirect('login')
    else:
        data_p = {}
        user0 = Profile.objects.get(slug=slug)
        user1 = Profile.objects.get(id=request.user.id)
        fr = Friend_Request.objects.filter(to_user=request.user)
        if user0:
            data_p['id_p'] = user0.id
            data_p['slug_p'] = user0.slug
            data_p['user_p'] = user0.user
            data_p['avatar_p'] = user0.avatar
            data_p['about_me_p'] = user0.about_me
            data_p['friends'] = user0.friends.all
            data_p['fr'] = fr
            if user1.friends.filter(id=user0.id).exists():
                data_p['bool_fr'] = True

            return render(request, "accounts/profile.html", data_p)



def send_friend_request(request, id):
    from_user = request.user
    to_user = User.objects.get(id=id)
    try:
        pr_user = to_user.profile.friends.get(username=from_user)
        return HttpResponseRedirect(reverse('profile_view', args = (from_user.profile.slug,)))
    except:
        Friend_Request.objects.get_or_create(from_user=from_user, to_user=to_user)
        return HttpResponseRedirect(reverse('profile_view', args = (from_user.profile.slug,)))


def decline_friend_request(request, id):
    fr = Friend_Request.objects.get(id=id)
    if fr:
        fr.delete()
        return HttpResponseRedirect(reverse('profile_view', args = (request.user.profile.slug,)))

def accept_friend_request(request, id):
    friend_request = Friend_Request.objects.get(id=id)
    if request.user == friend_request.to_user:
        user1 = friend_request.to_user
        user2 = friend_request.from_user
        user1.profile.friends.add(user2)
        user2.profile.friends.add(user1)
        friend_request.delete()
    return HttpResponseRedirect(reverse('profile_view', args = (user1.profile.slug,)))

def delete_friens(request, id):
    user1 = request.user
    user2 = User.objects.get(id=id)
    user1.profile.friends.remove(user2)
    user2.profile.friends.remove(user1)
    return HttpResponseRedirect(reverse('profile_view', args = (user1.profile.slug,)))

def chat(request, to_user):
    username = request.user
    id = username.id

    user0 = Profile.objects.get(id=to_user)
    user1 = Profile.objects.get(id=id)
    if user1.friends.filter(id=user0.id).exists():

        room_id0 = id / float(to_user) + float(to_user) / id
        room_id1 = id ** 0.5 + float(to_user) ** 0.5
        room_id = room_id0 + room_id1

        
        if request.method == 'GET':
            mesc = Message.objects.filter(room=room_id,check=False,username=to_user)
            mesc.update(check=True)
            messages = Message.objects.filter(room=room_id)
            return render(request, 'accounts/chat_ls.html', {'room_name': room_id, 'username': username, 'messages': messages, 'to_user': to_user,})
        if request.method == 'POST':
            Message.objects.filter(room=room_id).delete()
            return HttpResponseRedirect(reverse_lazy("chat_ls", kwargs={'to_user': to_user}))
