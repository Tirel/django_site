from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from blog.models import Product, Purchases
from django.contrib.auth.models import User
from blog.utils import get_price
import datetime
from django.utils.timezone import utc
import hashlib
from . import md5hash
import hashlib
import os

def cryptobox_rendering(request):
    boxID = 1234    # Your gourl cryptobox ID, https://gourl.io/editrecord/coin_boxes/0
    coin_name = 'bitcoin'
    public_key = 'your-gourl_public-key'
    private_key = 'your-gourl_private-key'
    webdev_key  = 'optional your-web-developer-key'
    amount = 0  # amount in bitcoins (Or another crybtocurrency
    period = '1 MINUTE'
    amountUSD = 5
    userID = request.user.username
    language = 'en'
    iframeID = 'iframeID'
    orderID = 'product-1'
    width = 530
    height = 230
    md5 = md5hash.hash(boxID, coin_name, public_key, private_key, webdev_key, amount,
                        period, amountUSD, userID, language, iframeID, orderID,
                        width, height)
    variables = {'boxID': boxID, 'coin_name': coin_name, 'public_key': public_key, 'webdev_key': webdev_key,
                 'amount': amount, 'period': period, 'amountUSD': amountUSD, 'userID': userID, 'language': language,
                 'iframeID': iframeID, 'orderID': orderID, 'width': width, 'height': height, 'hash': md5}
    return render(request, 'cryptobox_template.html', variables)

    
@csrf_exempt     # Very important! You need to allow a Foreign site (Gourl server) communicate with your server
def callback(request, *args, **kwargs):
    html = ""
    if request.method == 'POST':
        private_key = "Your-cryptobox-private-key"
        h = hashlib.sha512(private_key.encode(encoding='utf-8'))    # The incoming 'private_key' data from Gourl is sha512 encrypted
        private_key_hash = h.hexdigest()    # Hence, you need to hash your private key too for make security check
        if (request.POST.get('confirmed') == '0' and request.POST.get('box') == 'box-number' and
                request.POST.get('status') == 'payment_received' and
                request.POST.get('private_key_hash') == private_key_hash):     # Make the checks you need (Don't forget to check the 'private_key_hash')
            """
               Your code here for getting a unconfirmed payment    
            """
            html = "cryptobox_newrecord"     # Don't change this text. It's used by gourl server
        elif request.POST.get('confirmed') == '1':
            """
            Your code here for a payment confirmation
            """
            html = "cryptobox_updated"        # Don't change it
        else:
            """
            Your code here
            """
            html = "cryptobox_nochanges"    # Don't change it

    else:
        html = "Only POST Data Allowed"     # Don't change it

    return HttpResponse(html)

@csrf_exempt
def callback_cryptonator(request, *args, **kwargs):
    '''
    invoice_status
    <string>
    unpaid – не оплачен
    confirming - подтверждается
    paid – оплачен
    cancelled - отменен
    mispaid - сумам оплаты меньше суммы счета
    '''

    if request.method == 'POST':
        st = f"{request.POST.get('merchant_id')}&{request.POST.get('invoice_id')}&{request.POST.get('invoice_created')}&{request.POST.get('invoice_expires')}&{request.POST.get('invoice_amount')}&{request.POST.get('invoice_currency')}&{request.POST.get('invoice_status')}&{request.POST.get('invoice_url')}&{request.POST.get('order_id')}&{request.POST.get('checkout_address')}&{request.POST.get('checkout_amount')}&{request.POST.get('checkout_currency')}&{request.POST.get('date_time')}&{os.getenv('cryptonator_secret')}"

        hash_object = hashlib.sha1(st.encode('utf8'))
        hex_dig = hash_object.hexdigest()
        if request.POST.get('secret_hash') == hex_dig and request.POST.get('invoice_currency') == 'usd':
            open('logs/payments.log', 'a').write(str(request.POST)+'\n')
            #Проверка статуса оплаты
            if request.POST.get('invoice_status') == 'paid':
                # Сплитим order_id
                order_id_split = request.POST.get('order_id').split('---')
                prod_id = order_id_split[0]
                user_id = order_id_split[1]
                mounth = int(order_id_split[2])
                price = int(order_id_split[3])
                death_date = datetime.datetime.utcnow().replace(tzinfo=utc) + datetime.timedelta(days=mounth*31)
                prod_exists = Product.objects.filter(id=prod_id).exists()
                user_exists = User.objects.filter(id=user_id).exists()
                # Есть ли такой продукт и пользователь
                if prod_exists and user_exists:
                    price_a = get_price(int(Product.objects.get(id=prod_id).price), mounth)
                    invoice_amount = int(request.POST.get('invoice_amount'))
                    # Проверка цены в запросе и цены в базе
                    if invoice_amount == price and price_a == price:
                        prod_instance = Product.objects.get(id=prod_id)
                        user_instance = User.objects.get(id=user_id)
                        prod_id_base = prod_instance.id
                        user_id_base = user_instance.id
                        if not Purchases.objects.filter(purchase=prod_id_base,customer=user_id_base).exists():
                            if mounth <= 12:
                                Purchases.objects.create(purchase=prod_id_base,customer=user_id_base,death_date=death_date,price=price)
                            else:
                                Purchases.objects.create(purchase=prod_instance,customer=user_instance,subscription=False,price=price)

                            

                #{{ object.id }}---{{ user.id }}---+mounth+---' + price
            return HttpResponse('HTTP 200 OK')
"""
    customer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='purchases')
    purchase = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='purchases')
    subscription = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True,blank=True, null=True)
    death_date = models.DateTimeField(blank=True, null=True)
"""


def paid(request, *args, **kwargs):
    return HttpResponse('Payment passed')

def oopps(request, *args, **kwargs):
    return HttpResponse('Что-то пошло не так')