from django.urls import path
from . import views

urlpatterns = [
    path('gourl_callback/', views.callback, name='gourl_callback'),
    path('callback_cryptonator/', views.callback_cryptonator, name='callback_cryptonator'),
    path('paid/', views.paid, name='paid'),
    path('oopps/', views.oopps, name='oopps'),
]
