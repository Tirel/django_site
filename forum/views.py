from django.shortcuts import render
from django.views import generic
from .models import Topic, Post 
from accounts.models import Profile
from .forms import PostForm, TopicForm
from django.views.generic.edit import FormMixin
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect


from django.contrib.auth.mixins import LoginRequiredMixin #for Login access

from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger

class Search(generic.ListView):
    template_name = 'forum/main.html'
    
    def get_queryset(self):
        return Topic.objects.filter(visible=True, title__icontains=self.request.GET.get('q'))

class TopicList(generic.ListView):

    template_name = 'forum/main.html'
    model = Topic
    paginate_by = 15
    
    def get_context_data(self, **kwargs):
        context = super(TopicList, self).get_context_data(**kwargs) 
        list_exam = self.model.objects.filter(visible=True)
        paginator = Paginator(list_exam, self.paginate_by)


        p = Paginator(list_exam, self.paginate_by)
        page = self.request.GET.get('page')
        venues = p.get_page(page)
        context['venues'] = venues

        return context

    def get_queryset(self):
        queryset = self.model.objects.filter(visible=True).order_by('-created_on')
        return queryset


class TopicCreate(LoginRequiredMixin, generic.CreateView):
    template_name = 'forum/create.html'
    model = Topic
    form_class = TopicForm
    success_url = reverse_lazy('topic_edit_page')

    def get_context_data(self,**kwargs):
        kwargs['list_topic'] = self.model.objects.all().order_by('-id')
        return super().get_context_data(**kwargs)
    """
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if not self.request.user.is_superuser and not self.request.user.is_staff:
            return self.handle_no_permission()
        return kwargs
    """
    # Чтобы сохранить статью под данным пользователем 
    def form_valid(self,form):
        self.object = form.save(commit=False)
        self.object.author = self.request.user
        self.object.save()
        return super().form_valid(form)


class TopicEdit(LoginRequiredMixin, generic.UpdateView):
    model = Topic
    template_name = 'forum/edit.html'
    form_class = TopicForm
    success_url = reverse_lazy('topic_edit_page')

    # Чтобы редактировать только свои статьи
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.request.user != kwargs['instance'].author:
            return self.handle_no_permission()
        #if not self.request.user.is_superuser and not self.request.user.is_staff:
            #return self.handle_no_permission()
        return kwargs

class TopicEditPage(LoginRequiredMixin, generic.ListView):
    model = Topic
    template_name = 'forum/edit_page.html'

    def get_context_data(self, **kwargs):
        context = super(TopicEditPage, self).get_context_data(**kwargs) 
        list_topic = self.model.objects.filter(author=self.request.user)
        context['list_topic'] = list_topic
        return context

    def dispatch(self, request, *args, **kwargs):
        #if not self.request.user.is_superuser and not self.request.user.is_staff:
            #return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

class TopicDelete(LoginRequiredMixin, generic.DeleteView):
    model = Topic

    # Чтобы удалять только свои статьи
    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.request.user != self.object.author:
            return self.handle_no_permission()
        if not self.request.user.is_superuser and not self.request.user.is_staff:
            return self.handle_no_permission()
        self.object.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

class TopicDetail(generic.DetailView, FormMixin):
    model = Topic
    template_name = 'forum/topic_detail.html'
    form_class = PostForm
    paginate_by = 100

    #success_url = reverse_lazy('post_detail')

    def get_success_url(self,**kwargs):
        return reverse_lazy('forum_topic',kwargs={'pk': self.get_object().pk})

    def post(self,request,*args,**kwargs):
        form = self.get_form()
        if form.is_valid:
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self,form):
        self.object = form.save(commit=False)

        if self.request.POST.get("parent", None):
            form.parent = int(self.request.POST.get("parent"))
            self.object.is_child = True

        #self.object.post = self.get_object()
        self.object.author = self.request.user
        self.object.topic = self.model.objects.get(id=self.get_object().pk)
        self.object.save()
        return super().form_valid(form)

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)

        list_exam = self.model.objects.get(visible=True,id=self.kwargs['pk']).post_set.all()

        p = Paginator(list_exam, self.paginate_by)
        page = self.request.GET.get('page')
        venues = p.get_page(page)
        context['venues'] = venues

        return context
    """
    def get_context_data(self,**kwargs):
        topic = self.model.objects.get(id=self.get_object().pk)
        kwargs['list_post'] = Post.objects.filter(visible=True, topic=topic).order_by('-id')
        kwargs['topic'] = topic
        return super().get_context_data(**kwargs)
    """