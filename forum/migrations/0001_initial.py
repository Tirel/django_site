# Generated by Django 3.2.9 on 2021-11-04 16:48

import ckeditor_uploader.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import forum.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Topic',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(default='null', max_length=60)),
                ('description', ckeditor_uploader.fields.RichTextUploadingField(blank=True, max_length=5000, null=True)),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('rang', models.IntegerField(blank=True, default=0)),
                ('visible', models.BooleanField(default=True)),
                ('pictures', models.ImageField(blank=True, null=True, upload_to=forum.models.user_directory_path)),
                ('author', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('body', ckeditor_uploader.fields.RichTextUploadingField(max_length=3000)),
                ('visible', models.BooleanField(default=True)),
                ('is_child', models.BooleanField(default=False)),
                ('author', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='fpost', to=settings.AUTH_USER_MODEL)),
                ('parent', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='parent_post', to='forum.post', verbose_name='parent post')),
                ('topic', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='forum.topic')),
            ],
        ),
    ]
