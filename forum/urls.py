from . import views
from django.urls import path

urlpatterns = [
    path('search/', views.Search.as_view(), name='search_topik'),
    path('', views.TopicList.as_view(), name='forum_main'),
    path('topic/<pk>/', views.TopicDetail.as_view(), name='forum_topic'),
    path('edit_page/', views.TopicEditPage.as_view(), name='topic_edit_page'),
    path('edit/<int:pk>/', views.TopicEdit.as_view(), name='topic_edit'),
    path('create/', views.TopicCreate.as_view(), name='topic_create'),
    path('delete/<int:pk>/', views.TopicDelete.as_view(), name='topic_delete'),
]