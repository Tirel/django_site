from .models import Post, Topic
from django.forms import ModelForm, TextInput

class TopicForm(ModelForm):
    class Meta:
        model = Topic
        fields = ["title","description","pictures"]
        widgets = {
            "title": TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Введите название'})
        }


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ("body","parent","is_child")