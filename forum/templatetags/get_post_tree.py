from django import template
from django.utils.html import mark_safe
from django.http import HttpResponse

register = template.Library()

def get_children(qs_chid):
    res = []
    for post in qs_chid:
        c = {
            'id': post.id,
            'topic': post.topic,
            'body': post.body,
            'created_on': post.created_on.strftime('%Y-%m-%d %H:%m'),
            'author': post.author,
            'is_child': post.is_child,
            'parent_id': post.get_parent,
            'like': post.like.count(),
            'dislike': post.dislike.count()
        }
        if post.parent_post.exists():
            c['children'] = get_children(post.parent_post.all())
        res.append(c)
    return res

@register.filter()
def create_post_tree(qs):
    res = []
    for post in qs:
        c = {
            'id': post.id,
            'topic': post.topic,
            'body': post.body,
            'created_on': post.created_on.strftime('%Y-%m-%d %H:%m'),
            'author': post.author,
            'is_child': post.is_child,
            'parent_id': post.get_parent,
            'like': post.like.count(),
            'dislike': post.dislike.count()
        }
        if post.parent_post:
            c['children'] = get_children(post.parent_post.all())
        if not post.is_child:
            res.append(c)
    return res

@register.filter()
def post_filter(post_list):
    res = """
        <ul style="list-style-type:none;">
        {}
        </ul>
        """
    i = ''
    for post in post_list:
        i += """
            <li class="text-light">
                <div style="background-color: #302e3dc9;" class="card-body row mt-3 rounded card text-light">
                    <div class="row">
                        <div style="padding: 0%;" class="col-md-auto">
                            <img src="/media/{avatar}" class="img-fluid rounded" width="100" height="100">
                        </div>
                        <div class="col-md-auto">
                            <p style="color: aliceblue; margin-bottom: 0ch;">{author}</a>
                            <p style="margin-bottom: 0ch;">rang: {rang}</p>
                        </div>
                        <div class="col-md-auto">
                            <p class="text-muted" style="margin-bottom: 0ch;">{created_on}</p>
                            <p style="margin-bottom: 0ch;" class="card-text">{body}</p>
                        </div>
                    </div>
                    <div style="padding: 0ch;" class="card-footer">
                        <div class="row">
                            <div class="col" id="div_{post_id}">
                                like: {like}
                                dislike: {dislike}
                            </div>
                            <div class="col d-flex justify-content-end">
                                <button onclick="Answer('{post_id}')" type="button" class="r_e_p_l_y btn btn-outline-info btn-rounded" data-toggle="modal" data-target="#modal">
                                    Reply
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            """.format(
                post_id=post['id'],
                avatar=post['author'].profile.avatar,
                author=post['author'],
                rang=post['author'].profile.rang,
                body=post['body'],
                created_on=post['created_on'],
                like=post['like'],
                dislike=post['dislike']
                )
        if post.get('children'):
            i += post_filter(post['children'])
    return mark_safe(res.format(i))
