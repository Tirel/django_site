from django.contrib import admin
from forum.models import Topic, Post

class TopicAdmin(admin.ModelAdmin):
    list_display = ('id','title','visible','created_on','updated_on')
admin.site.register(Topic, TopicAdmin)

class PostAdmin(admin.ModelAdmin):
    list_display = ('id','author','topic','created_on')
admin.site.register(Post, PostAdmin)