from django.db import models
from accounts.models import Profile, Files
from blog.models import Like, Dislike
from django.contrib.auth.models import User

#for ckeditor
from ckeditor_uploader.fields import RichTextUploadingField
#for ckeditor

from django.contrib.contenttypes.fields import GenericRelation

def user_directory_path(instance, filename):
    return 'user_{0}/{1}'.format(instance.author.id, filename)

class Topic(models.Model):
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    title = models.CharField(max_length=60, default='null')
    description = RichTextUploadingField(max_length=5000, blank=True, null=True, config_name="default", external_plugin_resources=[('youtube','/static/ckeditor/youtube/','plugin.js',)],)
    #description = models.TextField(max_length=350, default='null')
    updated_on = models.DateTimeField(auto_now=True)
    created_on = models.DateTimeField(auto_now_add=True)
    rang = models.IntegerField(default=0, blank=True)
    visible = models.BooleanField(default=True)
    pictures = models.ImageField(upload_to=user_directory_path, null=True, blank=True)

    def get_only_parent(self):
        return self.post_set.filter(parent__isnull=True)

    def __str__(self):
        return self.title

class Post(models.Model):
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="fpost")
    topic = models.ForeignKey(Topic, on_delete=models.SET_NULL, null=True)
    created_on = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    body = RichTextUploadingField(max_length=3000, config_name="usersconfig")
    #body = models.TextField()
    visible = models.BooleanField(default=True)
    parent = models.ForeignKey(
        'self',
        default=None,
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='parent_post',
        verbose_name='parent post'
    )
    is_child = models.BooleanField(default=False)

    dislike = GenericRelation(Dislike)
    like = GenericRelation(Like)

    @property
    def get_parent(self):
        if not self.parent:
            return ''
        return self.parent

    def __str__(self):
        return f'{self.id}'

def user_directory_path_0(instance, filename):
    return "bg_forum" + '/' + filename
