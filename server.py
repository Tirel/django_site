import multiprocessing
import gunicorn.app.base
import os
import django

from channels.routing import get_default_application


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "site_for_bots.settings")
django.setup()

get_default_application()

def number_of_workers():
  return multiprocessing.cpu_count()

class StandaloneApplication(gunicorn.app.base.BaseApplication):

    def __init__(self, app, options=None):
        self.options = options or {}
        self.application = app
        super().__init__()

    def load_config(self):
        config = {key: value for key, value in self.options.items()
                  if key in self.cfg.settings and value is not None}
        for key, value in config.items():
            self.cfg.set(key.lower(), value)

    def load(self):
        return self.application


if __name__ == '__main__':
    options = {
        'bind': '%s:%s' % ('0.0.0.0', '8000'),
        'workers': number_of_workers(),
        'keyfile': 'data/cert/ca.key',
        'certfile': 'data/cert/ca.pem',
        'reload': True,
        'worker_class': 'uvicorn.workers.UvicornWorker',
    }
    StandaloneApplication('site_for_bots.asgi:application', options).run()