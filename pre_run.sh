python manage.py collectstatic --noinput
python manage.py makemigrations --noinput
python manage.py makemigrations accounts blog main forum auth --noinput
python manage.py migrate --noinput
python manage.py migrate --run-syncdb --noinput