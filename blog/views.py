from django.views import generic
from .models import Post as Blog_Post, Comments, Dislike, Like, Product, ProductWall, Purchases
from forum.models import Post as Forum_Post
from accounts.models import Profile
from .forms import PostForm, CommentForm
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin #for Login access
from django.views.generic.edit import FormMixin
from django.http import HttpResponseRedirect
from .likes import add_like, remove_like, is_fan, get_fans
from .utils import get_price
from datetime import datetime
from django.utils.timezone import utc

from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger

from django.contrib.contenttypes.models import ContentType

class PostListArticles(generic.ListView):
    model = Blog_Post
    template_name = "blog/articles.html"
    paginate_by = 20
    
    def get_context_data(self, **kwargs):
        context = super(PostListArticles, self).get_context_data(**kwargs) 
        list_exam = self.model.objects.all()
        p = Paginator(list_exam, self.paginate_by)
        page = self.request.GET.get('page')
        venues = p.get_page(page)
        
        context['venues'] = venues
        #context['list_exams'] = file_exams
        return context

    def get_queryset(self):
        queryset = self.model.objects.filter(news=0,status=1).order_by('-created_on')
        return queryset

class ArticlDetail(FormMixin, generic.DetailView):
    model = Blog_Post
    template_name = 'blog/post_detail.html'
    form_class = CommentForm

    def get_success_url(self,**kwargs):
        return reverse_lazy('post_detail',kwargs={'slug': self.get_object().slug})

    def post(self,request,*args,**kwargs):
        form = self.get_form()
        if form.is_valid:
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self,form):
        self.object = form.save(commit=False)
        self.object.object_id = self.get_object().pk
        self.object.content_type = ContentType.objects.get_for_model(self.model)
        self.object.author = self.request.user
        self.object.save()
        return super().form_valid(form)

class PostListProducts(generic.ListView):
    #queryset = Post.objects.filter(status=1).order_by('-created_on')
    template_name = 'blog/products.html'
    model = Product
    paginate_by = 20

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)

        slug = self.kwargs['slug']
        list_exam = self.model.objects.filter(visible=True,mode=slug)

        p = Paginator(list_exam, self.paginate_by)
        page = self.request.GET.get('page')
        venues = p.get_page(page)

        context['venues'] = venues           
        return context

    def get_queryset(self):
        slug = self.kwargs['slug']
        queryset = self.model.objects.filter(visible=True,mode=slug).order_by('-updated_on')
        return queryset

class ProductsDetail(FormMixin, generic.DetailView):
    model = Product
    template_name = 'blog/prod_detail.html'
    form_class = CommentForm
    paginate_by = 6
    paginate_c_by = 16

    def get_success_url(self,**kwargs):
        return reverse_lazy('product_detail',kwargs={'slug': self.get_object().slug})

    def post(self,request,*args,**kwargs):
        form = self.get_form()
        if form.is_valid:
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self,form):
        self.object = form.save(commit=False)
        self.object.object_id = self.get_object().pk
        self.object.content_type = ContentType.objects.get_for_model(self.model)
        self.object.author = self.request.user
        self.object.save()
        return super().form_valid(form)

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        if str(self.request.user) != 'AnonymousUser':
            customer = self.object.purchases.filter(customer=self.request.user)
            if customer:
                customer = customer.last()
                if customer.subscription == True:
                    if datetime.utcnow().replace(tzinfo=utc) <= customer.death_date:
                        context['proof'] = True
                else:
                    if str(customer) == str(self.request.user):
                        context['proof'] = True

        list_exam = self.model.objects.get(slug=self.kwargs['slug']).pwall.all().order_by('-created_on')

        p = Paginator(list_exam, self.paginate_by)
        page = self.request.GET.get('page')
        venues = p.get_page(page)
        context['venues'] = venues

        list_exam_c = self.model.objects.get(slug=self.kwargs['slug']).comm.all().order_by('-created_on')

        p_c = Paginator(list_exam_c, self.paginate_c_by)
        page_c = self.request.GET.get('page_c')
        venues_c = p_c.get_page(page_c)
        context['venues_c'] = venues_c

        price = self.model.objects.get(slug=self.kwargs['slug']).price

        
        context['price_month'] = round(price)
        context['price_3_months'] = get_price(price, 3)
        context['price_year'] = get_price(price, 12)
        context['price_forever'] = get_price(price, 13)
        return context


class PostDetail(FormMixin, generic.DetailView):
    model = Blog_Post
    template_name = 'blog/post_detail.html'
    form_class = CommentForm
    #success_url = reverse_lazy('post_detail')

    def get_success_url(self,**kwargs):
        return reverse_lazy('post_detail',kwargs={'slug': self.get_object().slug})

    def post(self,request,*args,**kwargs):
        form = self.get_form()
        if form.is_valid:
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self,form):
        self.object = form.save(commit=False)
        self.object.object_id = self.get_object().id
        self.object.content_type = ContentType.objects.get_for_model(self.model)
        self.object.author = self.request.user
        self.object.save()
        return super().form_valid(form)



def LikeView(request,pk):
    user = Profile.objects.get(user=pk)
    namePOST = ['comment_id_like','blogpost_id_dislike','comment_id_dislike','blogpost_id_like','forumpost_id_dislike', 'forumpost_id_like', 'prod_id_like', 'prod_id_dislike']
    nameMODEL = {'blogpost': Blog_Post, 'comment': Comments, 'forumpost': Forum_Post, 'prod': Product}
    nameMODEL0 = {'like': Like, 'dislike': Dislike}
    for i in namePOST:
        if request.POST.get(i):
            model = i.split('_')[0]
            model0 = i.split('_')[2]
            for nameI in nameMODEL:
                if model == nameI:
                    obj0 = get_object_or_404(nameMODEL[model], id=request.POST.get(i))
            for nameII in nameMODEL0:
                if model0 == nameII:
                    if is_fan(obj0, request.user, Like) == False and is_fan(obj0, request.user, Dislike) == False:
                        #add_like(obj0, request.user, nameMODEL0[model0])
                        if model0 == 'dislike':
                            add_like(obj0, request.user, Dislike)
                            user.rang-=1

                        if model0 == 'like':
                            add_like(obj0, request.user, Like)
                            user.rang+=1

                    elif is_fan(obj0, request.user, Like) == True and is_fan(obj0, request.user, Dislike) == False:
                        if model0 == 'dislike':
                            remove_like(obj0, request.user, Like)
                            add_like(obj0, request.user, Dislike)
                            user.rang-=2

                        if model0 == 'like':
                            remove_like(obj0, request.user, Like)
                            user.rang-=1

                    elif is_fan(obj0, request.user, Like) == False and is_fan(obj0, request.user, Dislike) == True:

                        if model0 == 'like':
                            remove_like(obj0, request.user, Dislike)
                            add_like(obj0, request.user, Like)
                            user.rang+=2

                        if model0 == 'dislike':
                            remove_like(obj0, request.user, Dislike)
                            user.rang+=1

    user.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    #return HttpResponseRedirect(reverse('post_detail', args=[str(slug)]))



class PostEditPageView(LoginRequiredMixin, generic.ListView):
    queryset = Blog_Post.objects.all()
    template_name = 'blog/edit_page.html'
    context_object_name = 'list_articles'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_superuser and not self.request.user.is_staff:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

class PostCreateView(LoginRequiredMixin, generic.CreateView):
    template_name = 'blog/create.html'
    model = Blog_Post
    form_class = PostForm
    success_url = reverse_lazy('blog_edit_page')

    def get_context_data(self,**kwargs):
        kwargs['list_articles'] = self.model.objects.all().order_by('-id')
        return super().get_context_data(**kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if not self.request.user.is_superuser and not self.request.user.is_staff:
            return self.handle_no_permission()
        return kwargs

    # Чтобы сохранить статью под данным пользователем 
    def form_valid(self,form):
        self.object = form.save(commit=False)
        self.object.author = self.request.user
        self.object.save()
        return super().form_valid(form)

class PostEditView(LoginRequiredMixin, generic.UpdateView):
    model = Blog_Post
    template_name = 'blog/edit.html'
    form_class = PostForm
    success_url = reverse_lazy('blog_edit_page')

    # Чтобы редактировать только свои статьи
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.request.user != kwargs['instance'].author:
            return self.handle_no_permission()
        if not self.request.user.is_superuser and not self.request.user.is_staff:
            return self.handle_no_permission()
        return kwargs

class PostDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = Blog_Post
    success_url = reverse_lazy('blog_edit_page')

    # Чтобы удалять только свои статьи
    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.request.user != self.object.author:
            return self.handle_no_permission()
        if not self.request.user.is_superuser and not self.request.user.is_staff:
            return self.handle_no_permission()
        self.object.delete()
        return redirect('blog_edit_page')
