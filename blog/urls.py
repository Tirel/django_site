from . import views
from django.urls import path

urlpatterns = [
#    path('', views.PostListArticles.as_view(), name='blog'),
    path('articls/', views.PostListArticles.as_view(), name='blog_articls'),
    path('articl/<slug>/', views.ArticlDetail.as_view(), name='post_detail'),
    path('products/<slug>/', views.PostListProducts.as_view(), name='blog_products'),
    path('product/<slug>/', views.ProductsDetail.as_view(), name='product_detail'),
    path('edit_page/', views.PostEditPageView.as_view(), name='blog_edit_page'),
    path('edit/<int:pk>/', views.PostEditView.as_view(), name='blog_edit'),
    path('create/', views.PostCreateView.as_view(), name='blog_create_articles'),
    path('delete/<int:pk>/', views.PostDeleteView.as_view(), name='blog_delete'),
    path('like/<int:pk>', views.LikeView, name='like_post'),
]