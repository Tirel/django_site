from modeltranslation.translator import register, TranslationOptions
from .models import Post, Product, ProductWall


@register(Post)
class PostTranslationOptions(TranslationOptions):
    fields = ('title', 'content')

@register(Product)
class ProductTranslationOptions(TranslationOptions):
    fields = ('title', 'description_text_only', )

@register(ProductWall)
class ProductTranslationOptions(TranslationOptions):
    fields = ('description', )