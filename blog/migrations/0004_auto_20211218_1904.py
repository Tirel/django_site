# Generated by Django 3.2.9 on 2021-12-18 15:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_product_mode'),
    ]

    operations = [
        migrations.AddField(
            model_name='purchases',
            name='created_on',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='purchases',
            name='death_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='purchases',
            name='subscription',
            field=models.BooleanField(default=True),
        ),
    ]
