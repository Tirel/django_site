from django.contrib import admin
from .models import Post, Comments, Like, Dislike, Product, ProductWall, Purchases
from modeltranslation.admin import TranslationAdmin

class PostAdmin(TranslationAdmin):
    list_display = ('title','slug','status','author','created_on')
    list_filter = ("status","news")
    search_fields = ['title', 'content']
    prepopulated_fields = {'slug': ('title',)}

class ProductAdmin(TranslationAdmin):
    list_display = ('title','slug','visible','author','created_on')
    list_filter = ("visible","created_on")
    search_fields = ['title', 'description_text_only']
    prepopulated_fields = {'slug': ('title',)}

class ProductWallAdmin(TranslationAdmin):
    list_display = ('prod','version','id','created_on')
    list_filter = ("created_on",)
    search_fields = ['description']

class PurchasesAdmin(admin.ModelAdmin):
    list_display = ('customer','price','purchase','subscription','death_date','created_on')

class LikeAdmin(admin.ModelAdmin):
    list_display = ('user','content_type','object_id','content_object')

class DislikeAdmin(admin.ModelAdmin):
    list_display = ('user','content_type','object_id','content_object')

admin.site.register(Dislike, DislikeAdmin)
admin.site.register(Like, LikeAdmin)
admin.site.register(Comments)
admin.site.register(Post, PostAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(ProductWall, ProductWallAdmin)
admin.site.register(Purchases, PurchasesAdmin)