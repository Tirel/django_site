from django.db import models
from django.conf import settings
#from django.contrib.auth.models import User

#for ckeditor
#from ckeditor.fields import RichTextUploadingField
from ckeditor_uploader.fields import RichTextUploadingField
#for ckeditor

# For likes model
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
# For likes model

from gdstorage.storage import GoogleDriveStorage
gd_storage = GoogleDriveStorage()

class Like(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
    related_name='likes',
    on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

class Dislike(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
    related_name='dislikes',
    on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

class Comments(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    content = models.TextField(verbose_name='Комментарий')
    status = models.BooleanField(verbose_name='Visible', default='True')

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    likes = GenericRelation(Like)
    dislikes = GenericRelation(Dislike)

class Post(models.Model):
    STATUS = (
        (0,"Draft"),
        (1,"Publish")
    )

    NEWS = (
        (0,"Articles"),
        (1,"Product"),
        (2,"HOME")
    )

    title = models.CharField(max_length=200, unique=True)
    slug = models.SlugField(max_length=200, unique=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete= models.CASCADE, blank=True, null=True)
    updated_on = models.DateTimeField(auto_now= True)
    content = RichTextUploadingField(blank=True, null=True, verbose_name='Контент', external_plugin_resources=[('youtube','/static/ckeditor/youtube/','plugin.js',)],)
    created_on = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(choices=STATUS, default=0)
    news = models.IntegerField(choices=NEWS, default=0)
    image_articles = models.ImageField(blank=True, null=True)

    comm = GenericRelation(Comments, related_query_name='post')
    dislikes = GenericRelation(Dislike)
    likes = GenericRelation(Like)

    @property
    def total_likes(self):
        return self.likes.count()

    class Meta:
        ordering = ['-created_on']

    def __str__(self):
        return self.title

class Product(models.Model):
    title = models.CharField(max_length=200)
    price = models.FloatField()
    mode = models.CharField(max_length=100, default='auto_gen')
    slug = models.SlugField(max_length=200, unique=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    ticker = models.CharField(max_length=20)
    description_text_only = models.TextField(blank=True, null=True)
    visible = models.BooleanField(default=True)
    image = models.ImageField(blank=True, null=True)

    updated_on = models.DateTimeField(auto_now=True)
    created_on = models.DateTimeField(auto_now_add=True)
    
    comm = GenericRelation(Comments, related_query_name='prod')
    dislikes = GenericRelation(Dislike)
    likes = GenericRelation(Like)

    def __str__(self):
        return self.slug

class ProductWall(models.Model):
    prod = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='pwall')
    version = models.IntegerField(blank=True, null=True)
    robot_file = models.FileField(upload_to='robots', storage=gd_storage)
    start_date = models.DateTimeField(blank=True, null=True)
    stop_date = models.DateTimeField(blank=True, null=True)
    amount_deals = models.PositiveIntegerField()
    type_optimization = models.CharField(max_length=50)
    quality_optimization = models.FloatField()
    created_on = models.DateTimeField(auto_now_add=True)
    description = RichTextUploadingField(blank=True, null=True, external_plugin_resources=[('youtube','/static/ckeditor/youtube/','plugin.js',)],)

    def __str__(self):
        return f'{self.id}'

class Purchases(models.Model):
    customer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='purchases')
    purchase = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='purchases')
    subscription = models.BooleanField(default=True)
    price = models.FloatField(blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True,blank=True, null=True)
    death_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return f'{self.customer}'
        
    class Meta:
        ordering = ['-created_on']