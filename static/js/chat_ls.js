
function Chat_ls(data) {
    var msg = JSON.parse(data);
    var chat_messages = document.getElementById("chat-messages");
    var roomName = msg.room_id;

    Chat_socket(roomName);

    console.log(msg);
    chat_messages.innerHTML = ""
    for (var i in msg) {
        msg010 = msg[i].msg.replace("\\n","\n")
        if (msg[i].side) {
            chat_messages.innerHTML += 
            '<div class="row text-light">'+
                '<p align="right" class="text-secondary">' + msg[i].user + '|' + msg[i].date + '</p>'+
                '<p align="right">' + msg010 + '</p>'+
            '</div>';
        }else {
            chat_messages.innerHTML += 
            '<div class="row text-light">'+
                '<p align="left" class="text-secondary">' + msg[i].user + '|' + msg[i].date + '</p>'+
                '<p align="left">' + msg010 + '</p>'+
            '</div>';
        }
    }
}

function Chat_socket(roomName) {
    roomName = roomName
    chatSocket = new WebSocket(
        'wss://'
        + window.location.host
        + '/ws/'
        + roomName
        + '/'
    );
    chatSocket.onmessage = function(e) {
        console.log('onmessage');
        var chat_messages = document.getElementById("chat-messages");
    
        const data = JSON.parse(e.data);
    
        if (data.message) {
            chat_messages.innerHTML += (
                '<div class="row text-light">'+
                    '<p align="right" class="text-secondary">' + data.username + '|' + new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds() + '</p>'+
                    '<p align="right">' + data.message + '</p>'+
                '</div>');
        } else {
            alert('The message is empty!');
        }
    };
        
    document.querySelector('#chat-message-submit').onclick = function(e) {
        const userName = JSON.parse(document.getElementById('json-username').textContent);
        const slugName = JSON.parse(document.getElementById('json-slug').textContent);
        const messageInputDom = document.querySelector('#chat-message-input');
        const message = messageInputDom.value;
    
        chatSocket.send(JSON.stringify({
            'message': message,
            'username': userName,
            //'avatar': avatarName,
            'slug': slugName,
            'room': roomName
        }));
    
        messageInputDom.value = '';
    };
};
