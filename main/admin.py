from django.contrib import admin
from .models import ex_data, Message, Background, About_us
from modeltranslation.admin import TranslationAdmin

class BackgroundAdmin(admin.ModelAdmin):
    list_display = ('id','bg','status')
    search_fields = ['id','bg', 'status']

class About_usAdmin(TranslationAdmin):
    list_display = ('id','title','content')
    search_fields = ['id','title','content']

class MessageAdmin(admin.ModelAdmin):
    list_display = ('room','username','content', 'date_added')
    search_fields = ['room','username','content', 'date_added']

admin.site.register(ex_data)
admin.site.register(Message, MessageAdmin)
admin.site.register(Background, BackgroundAdmin)
admin.site.register(About_us, About_usAdmin)

