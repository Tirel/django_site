from .models import ex_data
from django.forms import ModelForm, TextInput, Textarea


class ex_dataForm(ModelForm):
    class Meta:
        model = ex_data
        fields = ["title", "data"]
        widgets = {"title": TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Введите название'}),

            "data": Textarea(attrs={
            'class': 'form-control',
            'placeholder': 'Введите текст статьи'})}