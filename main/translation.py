from modeltranslation.translator import register, TranslationOptions
from .models import About_us


@register(About_us)
class CategoryTranslationOptions(TranslationOptions):
    fields = ('title', 'content')